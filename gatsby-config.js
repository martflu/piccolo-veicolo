module.exports = {
  pathPrefix: '/',
  siteMetadata: {
    title: 'Piccolo Veicolo',
    siteUrl: 'http://piccoloveicolo.at',
    description: 'Piccolo Veicolo - die Mobile Eventbar für Hochzeiten, Firmenfeiern, Geburtstagspartys und Events.',
  },
  plugins: [
    {
      resolve: 'gatsby-plugin-react-svg',
      options: {
        rule: {
          include: /svg/,
        },
      },
    },
    {
      resolve: 'gatsby-plugin-web-font-loader',
      options: {
        google: {
          families: ['Oswald'],
        },
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: 'Piccolo Veicolo',
        short_name: 'Piccolo Veicolo',
        description:
          'Piccolo Veicolo - die Mobile Eventbar für Hochzeiten, Firmenfeiern, Geburtstagspartys und Events.',
        lang: `de`,
        icon: `src/images/favicon.png`,
        start_url: `/`,
      },
    },
    'gatsby-plugin-postcss',
    'gatsby-plugin-typescript',
    'gatsby-plugin-preload-fonts',
    `gatsby-plugin-image`,
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sitemap`,
    {
      resolve: 'gatsby-plugin-zopfli',
      options: {
        extensions: ['css', 'html', 'js', 'svg'],
      },
    },
    `gatsby-plugin-offline`,
  ],
};
