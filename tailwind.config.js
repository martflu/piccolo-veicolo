module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {
      colors: {
        primary: '#e89cae',
        secondary: '#85b09a',
        secondarydark: '#517b65',
        background: '#fefafb',
        primaryrow: '#fcf0f3',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
