import Card from './components/card';
import ContactForm from './components/contact-form';
import Layout from './components/layout';
import Tier from './components/tier';

import { StaticImage } from 'gatsby-plugin-image';
import React from 'react';

const Home = () => {
  return (
    <Layout>
      <Card>
        <StaticImage
          className="mb-4"
          imgClassName="rounded-xl"
          src="../images/intro.jpg"
          alt="Piccolo Veicolo"
          placeholder="none"
        />
        <div>
          <h1 className="text-center">Piccolo Veicolo verleiht Ihren Gästen das italienische Lebensgefühl!</h1>
          <p>
            Sie möchten Ihre Gäste mit etwas Einzigartigem überraschen? Etwas, das man nie vergisst? Eine Besonderheit,
            über die noch lange gesprochen wird?
          </p>
          <p>Mit Piccolo Veicolo, der mobilen Eventbar, wird Ihnen das gelingen!</p>
          <p>
            Piccolo Veicolo ist eine umgebaute Piaggio Ape. Das dreirädrige Kultfahrzeug aus Italien, passend für das
            dolce Vita!
          </p>
          <p>
            Wir bieten Prosecco weiß und rosé, frisch gezapftes Bier und noch vieles mehr, für ein unvergessliches Fest.
          </p>
          <p>
            Piccolo Veicolo ist individuell einsetzbar. Für Hochzeiten, Firmenfeiern, Geburtstagspartys und Events. Zum
            Beispiel als Sektempfang vor der Kirche oder dem Standesamt.
          </p>
        </div>
      </Card>
      <Card>
        <h1>Leistungen und Preise</h1>
        <p>Alle Preise inkl. MwSt.</p>
        <p>
          Folgende Leistungen sind im Grundpreis von <b className="text-secondary whitespace-nowrap">€ 380,-</b>{' '}
          enthalten:
        </p>
        <ol className="mb-8">
          <li>Piccolo Veicolo bis zu 2 Stunden</li>
          <li>Auf- und Abbau</li>
          <li>Anfahrt der ersten 15 km ab Emmersdorf</li>
          <li>1 Stehtisch inklusive Husse</li>
          <li>Deko der Ape, Gläser, Servietten, Strohhalme, etc.</li>
        </ol>
        <table className="table-auto mb-8">
          <thead>
            <tr>
              <th>Leistungen</th>
              <th>Einzelpreise</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Bereitstellung des Piccolo Veicolo</td>
              <td>€ 380</td>
            </tr>
            <tr className="bg-primaryrow">
              <td>Prosecco weiss (750 ml)</td>
              <td>€ 19</td>
            </tr>
            <tr>
              <td>Prosecco rosé (750 ml)</td>
              <td>€ 19</td>
            </tr>
            <tr className="bg-primaryrow">
              <td>Bier vom Fass (20 l)</td>
              <td>€ 157,50</td>
            </tr>
            <tr>
              <td>Hugo</td>
              <td>zzgl. € 2</td>
            </tr>
            <tr className="bg-primaryrow">
              <td>Aperol Spritz</td>
              <td>zzgl. € 2</td>
            </tr>
            <tr>
              <td>Soda / Mineral (0,3 l)</td>
              <td>€ 2,50</td>
            </tr>
            <tr className="bg-primaryrow">
              <td>
                frische Früchte, Kräuter und Blüten
                <br />
                zum Verfeinern
              </td>
              <td>zzgl. € 1,5</td>
            </tr>
            <tr>
              <td>Alkoholfreie Getränke (0,3 l)</td>
              <td>€ 2,80</td>
            </tr>
            <tr className="bg-primaryrow">
              <td>Anfahrt (die ersten 15 km inkl.)</td>
              <td>€ 0,80 / km</td>
            </tr>
            <tr>
              <td>Personal</td>
              <td>€ 25 / h</td>
            </tr>
            <tr className="bg-primaryrow">
              <td>Stehtisch mit Husse (1 Stehtisch inkl.)</td>
              <td>€ 15</td>
            </tr>
          </tbody>
        </table>
        <p>Möchten Sie lieber ein individuelles Angebot?</p>
        <p>Wir stellen Ihnen gerne ihr Wunschpaket zusammen!</p>
      </Card>

      <div className="flex sm:flex-row flex-col space-y-8 sm:space-y-0 space-x-0 sm:space-x-8 mb-12">
        <Tier>
          <h2>Elegante</h2>
          <p className="text-center">Bis 50 Personen</p>
          <p className="text-center">Piccolo Veicolo für 2 Stunden</p>
          <p className="text-center">Auf- und Abbau</p>
          <p className="text-center">Zwei Team-Mitarbeiter für 2 Stunden</p>
          <p className="text-center">Gläser, Strohhalme, Servietten, Deko</p>
          <p className="text-center">Die ersten 15 km Anfahrt ab Emmersdorf</p>
          <p className="text-center">1 Stehtisch mit Husse</p>
          <p className="text-center">10 Flaschen Prosecco weiss und rosé</p>
          <p className="text-center">Salzgebäck</p>

          <div className="text-secondary text-xl">€ 680</div>
        </Tier>
        <Tier>
          <h2>Dolce Vita</h2>
          <p className="text-center">Bis 50 Personen</p>
          <p className="text-center">Piccolo Veicolo für 2 Stunden</p>
          <p className="text-center">Auf- und Abbau</p>
          <p className="text-center">Zwei Team-Mitarbeiter für 2 Stunden</p>
          <p className="text-center">Gläser, Strohhalme, Servietten, Deko</p>
          <p className="text-center">Die ersten 15 km Anfahrt ab Emmersdorf</p>
          <p className="text-center">1 Stehtisch mit Husse</p>
          <p className="text-center">5 Flaschen Prosecco weiss und rosé</p>
          <p className="text-center">1 Fass Bier</p>
          <p className="text-center">Salzgebäck</p>

          <div className="text-secondary text-xl">€ 743</div>
        </Tier>
        <Tier>
          <h2>Grande Festa</h2>
          <p className="text-center">Bis 80 Personen</p>
          <p className="text-center">Piccolo Veicolo für 2 Stunden</p>
          <p className="text-center">Auf- und Abbau</p>
          <p className="text-center">Drei Team-Mitarbeiter für 2 Stunden</p>
          <p className="text-center">Gläser, Strohhalme, Servietten, Deko</p>
          <p className="text-center">Die ersten 15 km Anfahrt ab Emmersdorf</p>
          <p className="text-center">2 Stehtische mit Hussen</p>
          <p className="text-center">10 Flaschen Prosecco weiss und rosé</p>
          <p className="text-center">1 Fass Bier</p>
          <p className="text-center">Salzgebäck</p>

          <div className="text-secondary text-xl">€ 912</div>
        </Tier>
      </div>
      <Card>
        <div className="grid grid-cols-1 sm:grid-cols-3 gap-4">
          <StaticImage imgClassName="rounded-xl" src="../images/p1.jpg" alt="Piccolo Veicolo" placeholder="none" />
          <StaticImage imgClassName="rounded-xl" src="../images/p2.jpg" alt="Piccolo Veicolo" placeholder="none" />
          <StaticImage imgClassName="rounded-xl" src="../images/p3.jpg" alt="Piccolo Veicolo" placeholder="none" />

          <StaticImage imgClassName="rounded-xl" src="../images/h1.jpg" alt="Piccolo Veicolo" placeholder="none" />
          <StaticImage imgClassName="rounded-xl" src="../images/h2.jpg" alt="Piccolo Veicolo" placeholder="none" />
          <StaticImage imgClassName="rounded-xl" src="../images/h3.jpg" alt="Piccolo Veicolo" placeholder="none" />

          <StaticImage imgClassName="rounded-xl" src="../images/h4.jpg" alt="Piccolo Veicolo" placeholder="none" />
          <StaticImage imgClassName="rounded-xl" src="../images/h5.jpg" alt="Piccolo Veicolo" placeholder="none" />
          <StaticImage imgClassName="rounded-xl" src="../images/h6.jpg" alt="Piccolo Veicolo" placeholder="none" />
        </div>
      </Card>
      <ContactForm />
    </Layout>
  );
};

export function Head() {
  return (
    <>
      <meta charSet="utf-8" />
      <title>Piccolo Veicolo</title>
      <meta name="title" content="Piccolo Veicolo" />
      <meta name="type" content="website" />
      <meta name="image" content="https://piccoloveicolo.at/icon.png" />
      <meta name="url" content="https://piccoloveicolo.at/" />
      <meta
        name="description"
        content="Piccolo Veicolo - die Mobile Eventbar für Hochzeiten, Firmenfeiern, Geburtstagspartys und Events."
      />
      <meta name="locale" content="de_AT" />
      <meta name="site_name" content="Piccolo Veicolo" />
    </>
  );
}

export default Home;
