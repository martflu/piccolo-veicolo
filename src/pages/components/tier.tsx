import React from 'react';

const Tier: React.FC = ({ children }) => {
  return <div className="flex flex-col items-center rounded-xl shadow-lg p-4 bg-white">{children}</div>;
};

export default Tier;
