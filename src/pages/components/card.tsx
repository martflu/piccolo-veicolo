import React from 'react';

const Card: React.FC = ({ children }) => {
  return (
    <div className="flex flex-col items-center justify-center rounded-xl shadow-lg p-4 sm:p-16 bg-background mb-12">
      {children}
    </div>
  );
};

export default Card;
