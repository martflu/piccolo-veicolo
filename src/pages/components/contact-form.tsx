import Card from './card';

import React from 'react';

const ContactForm = () => {
  return (
    <div className="mt-8 max-w-xl">
      <Card>
        <div className="bg-secondary text-lg text-white rounded-xl shadow-lg p-4 mb-8 text-center">
          Kontaktieren Sie mich unter <a href="mailto:info@piccoloveicolo.at">info@piccoloveicolo.at</a> <br />
          oder über das folgende Kontaktformular:
        </div>

        <form action="https://getform.io/f/2b7e2baf-9963-4c71-bf09-e05171845a0f" method="POST">
          <div className="flex flex-col mb-4">
            <label className="mb-2" htmlFor="email">
              E-Mail
            </label>
            <input id="email" className="border py-2 px-3" type="text" name="email" />
          </div>

          <div className="flex flex-col mb-4">
            <label className="mb-2" htmlFor="name">
              Name
            </label>
            <input id="name" className="border py-2 px-3" type="text" name="name" />
          </div>

          <div className="flex flex-col mb-4">
            <label className="mb-2" htmlFor="message">
              Nachricht
            </label>
            <textarea id="message" className="border py-2 px-3 w-80" name="message" />
          </div>
          <button
            className="bg-secondary transition duration-400 hover:bg-secondarydark text-white uppercase mx-auto px-8 py-2 rounded-xl"
            type="submit"
          >
            Senden
          </button>
        </form>
      </Card>
    </div>
  );
};

export default ContactForm;
