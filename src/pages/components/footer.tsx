import React from 'react';

const Footer = () => {
  return (
    <footer className="flex justify-center bg-secondary pb-16 w-full pt-16">
      <div className="text-white">
        <p>Piccolo Veicolo</p>
        <p>
          Eva Flucka
          <br />
          Kolomanistraße 7<br />
          3644 Emmersdorf
        </p>
      </div>
    </footer>
  );
};

export default Footer;
