import { StaticImage } from 'gatsby-plugin-image';
import React from 'react';

const Header = () => {
  return (
    <header className="flex justify-center bg-primary">
      <StaticImage
        className="bg-primary"
        src="../../images/logo.png"
        alt="Piccolo Veicolo"
        placeholder="none"
        width={400}
        height={400}
      />
    </header>
  );
};

export default Header;
