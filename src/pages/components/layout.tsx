import Footer from './footer';
import Header from './header';

import React from 'react';

const Layout: React.FC = ({ children }) => {
  return (
    <div className="flex flex-col items-center w-full">
      <Header />
      <div className="flex flex-col max-w-6xl items-center m-4 sm:m-8">{children}</div>
      <Footer />
    </div>
  );
};

export default Layout;
